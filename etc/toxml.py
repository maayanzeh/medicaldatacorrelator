#!/usr/bin/python

import dexml
from dexml import fields


class Doc(dexml.Model):
    id = fields.Integer()
    url = fields.String()
    title = fields.String()
    
    doc = fields.String(tagname='doc')

class Docs(dexml.Model):
    docs = fields.List(Doc)

if "__main__" == __name__:
	from sys import argv

	def usage_exit(msg):
		print "Usage: %s <id> <url> <title> <filepath>" % argv[0]
		print "Convert text file to a single xml container"
		print "Output is to stdout. Use pipe to direct it to a file\n"
		print "Error: %s\n" % msg
		exit()

	try:
		id, url, title, filepath = argv[1:]
	except ValueError:
		usage_exit("Invalid parameters received")

	try:
		text = file(filepath, "rb").read()
	except IOError:
		usage_exit("Couldn't read file '%s'" % filepath)

	d = Doc(id = id, url = url, title = title, doc = text)
	docs_object = Docs()
	docs_object.docs.append(d)

	print docs_object.render()
