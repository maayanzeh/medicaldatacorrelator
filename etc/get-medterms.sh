#!/bin/sh

for x in {a..z}; do
  wget http://www.medicinenet.com/script/main/alphaidx.asp?p="$x"_dict -O raw/$x;
  grep "^<li><a href=\"/script/main/art.asp.*</li>" raw/$x | sed 's/<li><a href=.*\">//g' | sed 's/<\/a><\/li>//g' > terms.$x;
done;
