# README #
### What is it? ###

The Medical Data Correlator is a machine-learning based document classifier, designed to point out medical related documents.

The classifier can be run distributively and thus can get large amount of data as its input.

### How do I get set up? ###

 * Requirements:
   - Apache Hadoop ver. 2.6
   - Spark ver. 1.3.1
   - Java ver. 8

 * Database configuration:
   Medical and non-medical XML data folders.

   The current database is about 7MB. this could be changed to improve the classifier's accuracy.
   

### How do I use the code? ###
 * To create the model:

   From the mediacorrelator root directory, run:

   spark-1.3.1-bin-hadoop2.6/bin/spark-submit --master local[4] --class mdc --driver-class-path lib/commons-cli-1.3.jar mdc.jar -m wiki-med-data -n wiki-non-med-data --chisqrt <chi-square value> -c <module>

   where:
    - <module> is one of SVM, NaiveBayes
    - <chi-square value> is the value that determines the feature selection.
   
   e.g.:
   spark-1.3.1-bin-hadoop2.6/bin/spark-submit --master local[4] --class mdc --driver-class-path lib/commons-cli-1.3.jar mdc.jar -m wiki-med-data -n wiki-non-med-data --chisqrt 1000 -c SVM


   note the model is based on: -m wiki-med-data -m wiki-non-med-data
   which simply means: -m <Medical-Xml-Directory> -n <Non-Medical-Xml-Directory>

 * To extract data from Wiki dumps to XML

   from the root of the repository:
   etc/WikiExtractor.py -o <Xml-Output-Path> <Wiki-Dump-Path>

   e.g:
   etc/WikiExtractor.py -o wiki19-out ../wiki-dumps-big-files/enwiki-20150403-pages-meta-current19.xml-p009225001p011125000.bz2

 * To convert a simple file to xml that the classifier can learn

   from the root of the repository:
   etc/toxml.py <id> <url> <title> <filepath>

   e.g.:
   etc/toxml.py "cacer_1" cancer.net Surgery "medical_docs/cancer/Surgery/cancer.net surgery.txt"

### Implementation Details ###
In order to build a corpus of big enough data with medical terms and articles, we processed the english Wikipedia dumps and filter through documents that are tagged with medical related subjects. See etc/WikiExtractor.py for the specific code that does it. 
We used the same approach to create a "negative" corpus, that is, a corpus of non medical data.

The above data was used to create a machine learning model using Spark pipelined. The tagged data was used as a supervised input for NaiveBayes and SVM models. The pipeline tokenizes the documents to bag of words and creates a TD-IDF vectors in order to train the model.

These models are used to predict documents from the ClueWeb dumps and calssify them as medical or non medical documents.

Data pipeline is figure: 


![Pipeline](readme/pipeline.png)


The program uses the TD-IDF vector created this way to create a vector for every document in the tagges data and train the model.
In order to predict a ClueWeb document, the program first strips all the HTML and non content related text from the document using [boilerpipe](https://github.com/kohlschutter/boilerpipe) library and uses the same procedure as above to build a corresponding TD-IDF vector. It then uses the model built above to create a binary classification.

### Statistics ###
The data below is on simple Spark configuration, on cluster of a single machine with 4 slaves:
 
* Building the models in a single machine cluster takes few seconds.

* Classify single ClueWeb dump file (about 650MB): take approximately 60 seconds

* Processing may consume full CPU usage on the slave machine, however, this is maintained by Spark.

* No additional disk space is needed over the manadatory database storage, and the binaries of this repository.

### Who do I talk to? ###

 - Avikam Agur - avikam@post
 - Maayan Zehavi - zehavm@post