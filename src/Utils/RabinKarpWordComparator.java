package Utils;
import java.math.BigInteger;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import org.apache.commons.lang3.tuple.ImmutablePair;

public class RabinKarpWordComparator implements IStringsComparator {
    private long Q;          // a large prime, small enough to avoid long overflow
    private int R;           // radix
    	
    private List<String> patterns; // TODO: remove
    private long[] patHashes;
    private long[] RMs;
    
    private int maxPatternLength;
    private int maxStateLength;
    private long[] stateHashes;
    private int[] statePoses;
	private String txt;
	private int last_J;

    public RabinKarpWordComparator(List<String> patterns) {
        this.patterns = patterns;
        R = 256;
        Q = longRandomPrime();
        
        int max = 0;
        for (String s : patterns) {
        	max = Math.max(max, s.length());
        }
        maxPatternLength = max;
      
        RMs = new long[maxPatternLength + 1];  
        // precompute R^(M-1) % Q for use in removing leading digit
        RMs[0] = 1;
        for (int i = 1; i <= maxPatternLength; i++)
           RMs[i] = (R * RMs[i-1]) % Q;
        
        patHashes = new long[patterns.size()];
        int j = 0;
        for (String p : patterns) {
        	patHashes[j] = hash(p, p.length());
        	j++;
        }
    } 

    // Compute hash for key[0..M-1]. 
    private long hash(String key, int M) { 
        long h = 0; 
        for (int j = 0; j < M; j++) 
            h = (R * h + key.charAt(j)) % Q; 
        return h; 
    } 

    // Las Vegas version: does pat[] match txt[i..i-M+1] ?
    private boolean check(String txt, int i) {
/*        for (int j = 0; j < M; j++) 
            if (pat.charAt(j) != txt.charAt(i + j)) 
                return false;*/ 
        return true;
    }

    // Monte Carlo version: always return true
    private boolean check(int i) {
        return true;
    }
    
    // a random 31-bit prime
    private static long longRandomPrime() {
        BigInteger prime = BigInteger.probablePrime(31, new Random());
        return prime.longValue();
    }
    
    private void AdvanceState(int j) {		
    	if (statePoses[j] >= txt.length()) {
    		stateHashes[j] = 0; // eliminate long hashes
    		return;
    	}
    	
        stateHashes[j] = (stateHashes[j] + Q - RMs[j-1]*txt.charAt(statePoses[j] - j) % Q) % Q; 
        stateHashes[j] = (stateHashes[j]*R + txt.charAt(statePoses[j])) % Q;
		statePoses[j]++;
    }
    
    private void AdvanceAllStates() {
    	for (int j=1; j <= maxStateLength; j++)
    		AdvanceState(j);
    }

	@Override
	public void Reset(String document) {
		this.txt = document;
		maxStateLength = Math.min(document.length(), maxPatternLength);
		stateHashes = new long[maxStateLength + 1];
		statePoses = new int[maxStateLength + 1];
		
        stateHashes[0] = 0;
        statePoses[0] = 0;
        for (int j = 1; j <= maxStateLength; j++) {
        	statePoses[j] = j;
        	stateHashes[j] = (R * stateHashes[j-1] + txt.charAt(j-1)) % Q;   	
        }
        this.last_J = 0;
    }

	@Override
	public Entry<String, Integer> next() {
		String p;
		if (txt.length() == 0)
			return null;
		
		while (statePoses[1] < txt.length()) {
			for (int j = last_J; j < patterns.size(); j++) {
				p = patterns.get(j);
				if (p.length() < maxStateLength && patHashes[j] == stateHashes[p.length()]) {
					last_J = j+1;
					return new ImmutablePair<String, Integer>(p, 0);
				}
			}
			last_J = 0;
			AdvanceAllStates();
		}

		return null;
	}
}
