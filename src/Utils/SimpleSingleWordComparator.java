package Utils;

import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;

public class SimpleSingleWordComparator implements IStringsComparator {
	private String pattern;
	private String document;
	private int last_pos = 0;
	
	public SimpleSingleWordComparator(String pattern) {
		this.pattern = pattern; 
	}
	
	@Override
	public Entry<String, Integer> next() {
		if (-1 != last_pos) {
			last_pos = StringUtils.indexOf(document, pattern, last_pos) + 1;
			return new ImmutablePair<String, Integer>(pattern, last_pos);
		} else {
			return null;
		}
	}

	@Override
	public void Reset(String document) {
		this.document = document;
	}

}
