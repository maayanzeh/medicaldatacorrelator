package Utils;

import java.util.Map.Entry;

public interface IStringsComparator {
	void Reset(String document);
	Entry<String, Integer> next();
}
