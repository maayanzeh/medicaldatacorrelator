package Utils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;

/**
 * 
 * @author avikam
 * Gets list of tokens and return vector representing them
 * The vector is given as map from coordinate to value.
 */
public class TDIDFVectorize implements Serializable {
	/**
	 * default 
	 */
	private static final long serialVersionUID = 1L;
	
	private BidiMap<String, Integer> featuresMap;
	private Map<String, Double> IDFs;
	
	public TDIDFVectorize (Map<String, Double> IDFs) {
		this.IDFs = IDFs;
		featuresMap = new DualHashBidiMap<String, Integer>();
		int j=0;
		for(Map.Entry<String, Double> idf : IDFs.entrySet()) {
			if (idf.getKey() != "")
				featuresMap.put(idf.getKey(), j++);
		}
	}
	
	public Map<Integer, Double> toVector(Map<String, Integer> docuemntTokensOccurences) {
		
		Map<Integer, Double> ret = new HashMap<Integer, Double>();
		int totalDocToken = docuemntTokensOccurences.size();
		
		for (Map.Entry<String, Integer> tokenOccurences : docuemntTokensOccurences.entrySet()) {
			String token = tokenOccurences.getKey();
			Integer occurences = tokenOccurences.getValue();
			if (indexOf(token) != null) { /* filter out tokens that we cleaned */
				ret.put(indexOf(token), 
						calcTDIDF(totalDocToken, occurences ,token));
			}
		}
		
		return ret;
	}
	
	public int getDimension() {
		return featuresMap.size();
	}
	
	private Integer indexOf(String token) {
		return featuresMap.get(token);
	}
	
	private double calcTDIDF(int totalTokens, int tokenOccurencesInDoc, String term) {
		if (IDFs.get(term) == null) {
			throw new NullPointerException("term must not be null!!");
		}
		return calcTDIDF(totalTokens, tokenOccurencesInDoc, IDFs.get(term));
	}
	
	private double calcTDIDF(int totalTokens, int tokenOccurencesInDoc, double termIdf) {
		return termIdf * (((double) tokenOccurencesInDoc) / (double)totalTokens);
	}
}
