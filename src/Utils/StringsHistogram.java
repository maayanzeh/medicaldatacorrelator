package Utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class StringsHistogram <C extends IStringsComparator> {
	private final C comparator;
	private Map<String, Integer> histogram;
	
	public StringsHistogram(C comparator) {
		this.comparator = comparator;
		this.histogram = new HashMap<String, Integer>();
	}
	
	public void addHistogram(String document) {
		comparator.Reset(document);
		
		Entry<String, Integer> res;
		while (null != (res = comparator.next())) {
				Integer old = histogram.getOrDefault(res.getKey(), 0);
				histogram.put(res.getKey(), old + 1);
		}
	}
	
	public Map<String, Integer> getMap() {
		return this.histogram;
	}
	
}
