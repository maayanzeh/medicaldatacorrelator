package Utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class DumpObjects {
	public static void Dump(Object obj, String outfilePath){
		try {
	       FileOutputStream fileOut = new FileOutputStream(outfilePath);
	       ObjectOutputStream out = new ObjectOutputStream(fileOut);
	       
	       out.writeObject(obj);
	       out.close();
	       fileOut.close();
	    } catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Object Load(String outfilePath) {
		try
		{
			FileInputStream fileIn = new FileInputStream(outfilePath);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			
			Object obj = in.readObject();
			in.close();
			fileIn.close();
			
			return obj;
		} catch(IOException i) {
			i.printStackTrace();
			return null;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
}
