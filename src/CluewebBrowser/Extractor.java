package CluewebBrowser;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import edu.cmu.lemurproject.WarcRecord;

public abstract class Extractor {
	protected Repository repository;
	
	static public interface RecordFilter {
		boolean Predicate(WarcRecord w);
	}
	
	public Extractor(Repository rep) {
		this.repository = rep;
	}
	
	protected List<WarcRecord> TryFindAllRecords(RecordFilter filter) {
		List<WarcRecord> foundRecords = new ArrayList<WarcRecord>();
		
		for (RepositoryFile repFile : repository.GetRepositoryFiles()) {
			DataInputStream stream = repFile.TryCreateDataInputStream();
			if (stream == null) {
				System.err.println("No repository content for " + repFile.GetFileName());
				continue;
			}

			WarcRecord record;
			while ((record = TryGetNextRecord(stream)) != null) {
			if (filter.Predicate(record))
				foundRecords.add(record);
			}
		}
		
		return foundRecords;
	}
	
	protected WarcRecord TryFindFirstRecord(RecordFilter filter) {
		for (RepositoryFile repFile : repository.GetRepositoryFiles()) {
			DataInputStream stream = repFile.TryCreateDataInputStream();
			if (stream == null) {
				System.err.println("No repository content for " + repFile.GetFileName());
				continue;
			}

			WarcRecord record;
			while ((record = TryGetNextRecord(stream)) != null) {
			if (filter.Predicate(record))
				return record;
			}
		}
		
		return null;
	}
	
	protected WarcRecord TryGetNextRecord(DataInputStream stream) {			
		try {
			WarcRecord record = WarcRecord.readNextWarcRecord(stream);
			return record;
			
		} catch (IOException e) {
			System.err.println("Error in repository file");
			System.err.println(e.getMessage());
			System.err.println(e.getStackTrace());
		}
		
		return null;
	}
	
	public abstract String Extract(UUID uuid);
	public abstract String Extract(String url);
	public abstract Map<String, String> Extract(List<String> uris);
	
}
