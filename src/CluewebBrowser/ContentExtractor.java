package CluewebBrowser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.ArticleExtractor;

public class ContentExtractor extends FullExtractor{
	public ContentExtractor(Repository rep) {
		super(rep);
	}

	private String extractContent(String s) {
		if (s == null)
			return null;	
		
		try {
			return ArticleExtractor.INSTANCE.getText(s);
		} catch (BoilerpipeProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	private Map<String, String> extractContent(Map<String, String> fullContents) {
		if (fullContents == null)
			return null;
		
		Map<String, String> contents = new HashMap<String, String>();
		for (Map.Entry<String, String> fullContent : fullContents.entrySet()) {
			try {
				contents.put(fullContent.getKey(), ArticleExtractor.INSTANCE.getText(fullContent.getValue()));
			} catch (BoilerpipeProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return contents;
	}
	
	@Override
	public String Extract(UUID uuid) {
		return extractContent(super.Extract(uuid));
	}
	
	@Override
	public String Extract(String url) {
		return extractContent(super.Extract(url));
	}
	
	@Override
	public Map<String, String> Extract(final List<String> uris) {
		return extractContent(super.Extract(uris));
	}
}