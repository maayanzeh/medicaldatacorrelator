package CluewebBrowser;

public class ExtractorFactory <E extends Extractor> {
	private Class<E> extractorClass;
	public ExtractorFactory(Class<E> extractorType) {
		this.extractorClass = extractorType;
	}
	
	public Extractor CreateExtractor(String[] files) throws InstantiationException, IllegalAccessException {
		Extractor e =  extractorClass.newInstance();
		
		return e;
	}
}
