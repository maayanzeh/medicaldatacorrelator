package CluewebBrowser;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class RepositoryFile {
	
	private String fileName;
	
	public RepositoryFile(String fileName) {
		this.fileName = fileName;
	}
	public String GetFileName() {
		return fileName;
	}
	
	public DataInputStream TryCreateDataInputStream() {
		try {
			DataInputStream ret = new DataInputStream(new FileInputStream(fileName));
			return ret;
		} catch (FileNotFoundException e) {
			return null;

		}
	}
}
