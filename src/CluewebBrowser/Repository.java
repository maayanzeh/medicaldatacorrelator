package CluewebBrowser;

import java.util.List;

public interface Repository {
	public List<RepositoryFile> GetRepositoryFiles(); 
}
