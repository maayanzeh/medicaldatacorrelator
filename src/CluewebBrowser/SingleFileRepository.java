package CluewebBrowser;

import java.util.ArrayList;
import java.util.List;

public class SingleFileRepository implements Repository {

	private List<RepositoryFile> singleFile;
	
	public SingleFileRepository(String fileName) {
		singleFile = new ArrayList<RepositoryFile>(1);
		
		singleFile.add(new RepositoryFile(fileName));
	}
	
	@Override
	public List<RepositoryFile> GetRepositoryFiles() {
		// Return a copy
		return singleFile.subList(0, 1);
	}

}
