package CluewebBrowser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import edu.cmu.lemurproject.WarcRecord;

public class FullExtractor extends Extractor {	
	
	public FullExtractor(Repository rep) {
		super(rep);
	}

	@Override
	public String Extract(final UUID uuid) {
		// TODO: Check if uuid in cache
		WarcRecord found = TryFindFirstRecord(new RecordFilter() {
			public boolean Predicate(WarcRecord w) { return w.getWarcUUID().equals(uuid); }
		});
		
		if (found == null)
			return null;
		return found.getContentUTF8();
	}

	@Override
	public String Extract(final String uri) {
		// TODO: Check if url in cache
		WarcRecord found = TryFindFirstRecord(new RecordFilter() {
			public boolean Predicate(WarcRecord w) {
				String w_uri = w.getHeaderMetadataItem("WARC-Target-URI");
				return w_uri != null && w_uri.equals(uri);
			}
		});
		
		if (found == null)
			return null;
		return found.getContentUTF8();
	}
	
	@Override
	public Map<String, String>  Extract(final List<String> uris) {
		List<WarcRecord> found = TryFindAllRecords(new RecordFilter() {
			public boolean Predicate(WarcRecord w) {
				String w_uri = w.getHeaderMetadataItem("WARC-Target-URI");
				if (w_uri != null)
					for (String uri : uris) if (w_uri.equals(uri)) return true;
				return false;
			}
		});
		
		Map<String, String> contents = new HashMap<String, String>();
		if (found == null)
			return contents;
		
		for (WarcRecord w : found)
			contents.put(w.getHeaderMetadataItem("WARC-Target-URI"), w.getContentUTF8());
		
		return contents;
	}
	
	
}
