import java.io.Serializable;

import org.apache.spark.mllib.classification.ClassificationModel;
import org.apache.spark.mllib.feature.ChiSqSelectorModel;
import Utils.TDIDFVectorize;


public class ClassifierVectorModel<C extends ClassificationModel> implements Serializable{

	/**
	 * default
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * model
	 */
	public C model;
	
	/**
	 * Factory for vectors
	 */
	public TDIDFVectorize vectorFactory;
	
	public ChiSqSelectorModel chiSqrModel;
	
	public ClassifierVectorModel(C model, ChiSqSelectorModel chiSqrModel, TDIDFVectorize factory){
		this.model = model;
		this.chiSqrModel = chiSqrModel;
		this.vectorFactory = factory;
	}

}
