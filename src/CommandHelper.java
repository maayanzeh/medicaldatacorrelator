import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;


public class CommandHelper {
	private CommandLine cmd;
	
	public CommandHelper(String[] args) {			
		Options options = new Options();
		
		Option.Builder builder = Option.builder();
		
		options.addOption("m", true, "medical tagged XML path");
		options.addOption("n", true, "non medical tagged XML path");
		
		
		options.addOption("l", true, "Load train data");
		options.addOption("d", true, "Dump train data");

		options.addOption(builder.longOpt("nostem")
				 .hasArg(false)
			 	 .desc("Specify this option if you DON'T want to stem the tokens. Stemming is done in english only.")
			 	 .build());
		
		options.addOption(builder.longOpt("chisqrt")
				 .hasArg(true)
			 	 .desc("Chi-Square featues threshold")
			 	 .type(Number.class)
			 	 .build());
		
		options.addOption(Option.builder("c")
				 .longOpt("classifier")
				 .hasArg(true)
				 .argName("classifier algorithm name")
			 	 .desc("NaiveBayes or SVM")
			 	 .required()
			 	 .build());
		
		options.addOption(builder.longOpt("pwarc")
								 .hasArg(true)
							 	 .desc("Predict WARC file")
							 	 .build());
		
		options.addOption(builder.longOpt("pxml")
				 				 .hasArg(true)
			 				 	 .desc("Predict XML file")
			 				 	 .build());
		
		
		options.addOption(builder.longOpt("txml")
				 .hasArg(true)
			 	 .desc("Specify XML of wiki files you want tokenize for testings")
			 	 .build());
		
		
		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		try
		{
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			e.printStackTrace();
			formatter.printHelp( "mdc", options );
			throw new IllegalArgumentException();
		}
		
		if ((cmd.hasOption("m") && !cmd.hasOption("n")) || (!cmd.hasOption("m") && cmd.hasOption("n"))) {
			System.err.println("error: used -m\\-n without -n\\-m");
			formatter.printHelp("mdc", options );
			throw new IllegalArgumentException();			
		}
		
		if (cmd.hasOption("l") && (cmd.hasOption("n") || cmd.hasOption("m"))) {
			System.err.println("error: used -l and (-m or -n)");
			formatter.printHelp("mdc", options );
			throw new IllegalArgumentException();			
		}
		
		if ((!cmd.hasOption("l") && !cmd.hasOption("n")) && !cmd.hasOption("m")) {
			System.err.println("error: no train, and no load. choose train (-m, -n) or load (-l)");
			formatter.printHelp("mdc", options );
			throw new IllegalArgumentException();			
		}
		
	}
	
	public String GetMedicalPath() {
		return cmd.getOptionValue("m");
	}
	
	public String GetNonMedicalPath() {
		return cmd.getOptionValue("n");
	}
	
	public String GetLoadKnownledgeBasePath() {
		return cmd.getOptionValue("l");
	}
	
	public boolean ShouldPredictXML() {
		return cmd.hasOption("pxml");
	}
	
	public boolean ShouldPredictWARC() {
		return cmd.hasOption("pwarc");
	}
	
	public String GetPredictXmlPath() {
		if (cmd.hasOption("pxml")) {
			return cmd.getOptionValue("pxml");
		}
		return null;
	}
	
	public String GetPredicWarcPath() {
		if (cmd.hasOption("pwarc")) {
			return cmd.getOptionValue("pwarc");
		}
		return null;
	}
	
	public boolean ShouldTrian() {
		return !cmd.hasOption("l");
	}
	
	public boolean ShouldDumpTrain() {
		return cmd.hasOption("d");
	}
	
	public String GetDumpTrainDataPath() {
		return cmd.getOptionValue("d");
	}
	
	public boolean ShouldTokenizeXml() {
		return cmd.hasOption("txml");
	}
	
	public String GetTokenizeXmlPath() {
		return cmd.getOptionValue("txml");
	}
	
	public boolean ShouldStem() {
		return !cmd.hasOption("nostem");
	}
	
	public int getChiSqrThreshold(int defaultValue) {
		if (!cmd.hasOption("chisqrt")) {
			return defaultValue;
		}

		try {
			Number n = (Number)cmd.getParsedOptionValue("chisqrt");
			return n.intValue();
		} catch (ParseException e) {
			throw new IllegalArgumentException("chisqrt is an integer!, and not '" + cmd.getOptionValue("chisqrt") + "'");
		}
	}
	
	public String GetClassificationAlgorithm() {
		String alg = cmd.getOptionValue("c");
		if ((alg.compareToIgnoreCase("NaiveBayes") != 0) && (alg.compareToIgnoreCase("SVM") != 0)){
			throw new IllegalArgumentException("Classification algorithm is either NaiveBayes or SVM, not " + alg);
		}
		
		return alg;
	}
}