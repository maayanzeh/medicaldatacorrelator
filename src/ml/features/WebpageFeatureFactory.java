package ml.features;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.io.StringReader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

import ml.dataobjects.TokenizedDocument;
import ml.dataobjects.WebpageDocument;

public class WebpageFeatureFactory {
	private static CharArraySet Stopwords = ParseStopwordList();
	private static final Analyzer enAnalyzer = new EnglishAnalyzer(Stopwords);
	private static final Analyzer standardAnalyzer = new StandardAnalyzer (Stopwords);
	
	private static CharArraySet ParseStopwordList() {
		InputStream in = WebpageFeatureFactory.class.getResourceAsStream("/resources/stopwords.txt");
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		List<String> stopwords = new ArrayList<String>();
		
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				stopwords.add(line.toLowerCase());
			}
				
		} catch (IOException e) {
			System.out.println("Error with stopwords");
			System.exit(1);
		}
		
		return new CharArraySet(stopwords, true);
	}
	
    /**
     * Counts the number of occurrences of the keywords inside the text.
     * 
     * @param keywordArray
     * @return 
     */
    public static void addFeatures(Map<String, Integer> counts, List<String> keywordArray) {
        Integer counter;
        for(String s : keywordArray) {
            counter = counts.getOrDefault(s, 0);
            counts.put(s, ++counter); //increase counter for the keyword
        }
    }
    
    public static TokenizedDocument buildFeaturedDocument (WebpageDocument webPage, boolean shouldStem) {
    	return buildFeaturedDocument(webPage, null, shouldStem);
    }
    
    /**
     * Tokenizes the document and returns a Document Object.
     * 
     * @param text
     * @return 
     */
    public static TokenizedDocument buildFeaturedDocument (WebpageDocument webPage, String category, boolean shouldStem) {
    	
    	Map<String, Integer> features = new HashMap<>();    	
        
    	Analyzer analyzer = shouldStem ? enAnalyzer : standardAnalyzer;
    	
    	//if (webPage.domain != null)
    	//	addFeatures(features, preprocessDomain(webPage.domain));
    	if (webPage.path != null)
    		addFeatures(features, preprocessPath(webPage.path));
    	
        try {
			addFeatures(features, preprocessContent(webPage.content, analyzer));
		} catch (IOException e) {
			System.out.println("IO Error when proccessing content");
			e.printStackTrace();
			return null;
		}
        
        
        TokenizedDocument doc = new TokenizedDocument(webPage.path, category, features);
        return doc;
    }
    
    private static List<String> preprocessDomain(String domain){
    	String[] tokens = domain.replaceFirst("http://", "").replaceFirst("www", "").split("\\.");
    	List<String> domainFeatures = new ArrayList<String>();
    	
    	for (int j=0; j<tokens.length; j++) {
    		if (tokens[j].length() > 0)
    			domainFeatures.add("DOMIAN_" + tokens[j]);
    	}
    	
    	return domainFeatures;
    }
    
    private static List<String> preprocessPath(String path){
    	String[] res = path.split("/");
    	List<String> l = new ArrayList<String>();
    	
    	for (int j=0; j<res.length; j++) {
    		if (res[j].length() > 0)
    			l.add("PATH_" + res[j]);
    	}
    	
    	return l;
    }
    
    
    public static List<String> preprocessContent(String text, Analyzer analyzer) throws IOException{
    	List<String> l = new ArrayList<String>();
    	
    	StringReader reader = new StringReader(text);
    	TokenStream tokenStream = analyzer.tokenStream("content", reader);
    	CharTermAttribute termAttr = tokenStream.addAttribute(CharTermAttribute.class);
    	
    	try {
    		tokenStream.reset();
    		while (tokenStream.incrementToken()) {
    			String term = termAttr.toString();
    			// filter out some numbers, small terms ... 
    			if (term.length() >= 2 && !term.matches("\\d+")) {
    				l.add(term);    				
    			}
    		}
    	} finally {
    		tokenStream.close();
    	}
    	
    	return l;
    }
	
}
