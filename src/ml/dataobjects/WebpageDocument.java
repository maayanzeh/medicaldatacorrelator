package ml.dataobjects;

import java.io.Serializable;

public class WebpageDocument implements Serializable {	    
	private static final long serialVersionUID = 1L;
	
	/* Some information */
    public String domain;
    public String path;
    public String content;
    
    public String containerId;
    
    public WebpageDocument(String domain, String path, String content) {
    	this.domain = domain;
    	this.path = path;
    	this.content = content;
    	
    	this.containerId = "";
    }
    
    public WebpageDocument(String domain, String path, String content, String containerId) {
    	this.domain = domain;
    	this.path = path;
    	this.content = content;
    	this.containerId = containerId;
    }
}
