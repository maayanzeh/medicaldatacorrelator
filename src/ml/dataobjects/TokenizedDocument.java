package ml.dataobjects;
import java.io.Serializable;
import java.util.Map;

/**
 * The Document Object represents the texts that we use for training or 
 * prediction as a bag of words.
 * 
 * @author Vasilis Vryniotis <bbriniotis at datumbox.com>
 * @see <a href="http://blog.datumbox.com/developing-a-naive-bayes-text-classifier-in-java/">http://blog.datumbox.com/developing-a-naive-bayes-text-classifier-in-java/</a>
 */
public class TokenizedDocument implements Serializable{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * List of features
     */
    public Map<String, Integer> features;
    
    /**
     * The class of the document
     */
    public String category;
    
    /**
     * Some identifier can be whatever...
     */
    public String docId;
    
    /**
     * Document constructor
     */
    public TokenizedDocument(String docId, String category, Map<String, Integer> features) {
    	this.features = features;
    	this.category = category;
    	this.docId = docId;
    }
}