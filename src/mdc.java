import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import ml.dataobjects.TokenizedDocument;
import ml.dataobjects.WebpageDocument;
import ml.features.WebpageFeatureFactory;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.ArticleExtractor;
import Utils.TDIDFVectorize;
import edu.cmu.lemurproject.WarcRecord;

import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.*;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.classification.ClassificationModel;
import org.apache.spark.mllib.classification.SVMWithSGD;
import org.apache.spark.mllib.classification.NaiveBayes;
import org.apache.spark.mllib.feature.ChiSqSelector;
import org.apache.spark.mllib.feature.ChiSqSelectorModel;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;

import scala.Tuple2;


public class mdc {
	//private static Configuration globalConf = new Configuration();
	
	// constants
	private static float Medical = 0;
	private static float Other = 1;
	private static float Unsupervised = 2;
	
	// environment:
	private static JavaSparkContext sparkConf;
	
	
	// params:
	private static ChiSqSelector chiSqrSelector;
	private static final int chiSqrDefaultSelector = 20000;
	private static final double testSample = 0.05;

	public static void main(String[] args) throws Exception {
		//StartJob();
		CommandHelper cmd;
		try {
			cmd = new CommandHelper(args);
		} catch (IllegalArgumentException e1) {
			System.err.println("Bad usage. leaving");
			return;
		}
		
		TokenizeIfNeeded(cmd);
		
		// yes this global is bad but this is also life:
		chiSqrSelector = new ChiSqSelector(cmd.getChiSqrThreshold(chiSqrDefaultSelector));
		
		SparkConf conf = new SparkConf().setAppName("Simple Application");
		
		sparkConf = new JavaSparkContext(conf);
		
		String classificationAlgorithm = cmd.GetClassificationAlgorithm();
		
		// Acquire Naive Bayes model
		ClassifierVectorModel<?> naiveBayes;
		System.out.println("Classifying with " + classificationAlgorithm);
		if (classificationAlgorithm.compareToIgnoreCase("NaiveBayes") == 0) {
			naiveBayes = SparcTrain(cmd, (trainingSet -> NaiveBayes.train(trainingSet.rdd(), 1.0)) );
		} else if (classificationAlgorithm.compareToIgnoreCase("SVM") == 0) {
			naiveBayes = SparcTrain(cmd, (trainingSet -> SVMWithSGD.train(trainingSet.rdd(), 100)) );
		} else {
			throw new IllegalStateException("Unknown classification algorithm");
		}
		
		PredictIfNeeded(cmd, naiveBayes);

		
	    sparkConf.close();
	}

	private static JavaRDD<TokenizedDocument> MapWebpagesToTokenizedDocuemnts(List<WebpageDocument> webDocs, boolean shouldStem) {
		// load to cluster
		JavaRDD<WebpageDocument> distDocs = sparkConf.parallelize(webDocs);
		// map them to tokenized docs
	    return distDocs
	    	   .map(new Function<WebpageDocument, TokenizedDocument>() {
	    		   		public TokenizedDocument call(WebpageDocument doc) throws Exception {
    		   				return WebpageFeatureFactory.buildFeaturedDocument(doc, shouldStem);
	    		   		}
	    	   });
	}
	
	private static JavaRDD<LabeledPoint> MapTokenizedDocumentToVectors(double label, TDIDFVectorize vectorize, JavaRDD<TokenizedDocument> docsRDD) {
		return 
		docsRDD
		.map(new Function<TokenizedDocument, LabeledPoint>() {
			public LabeledPoint call(TokenizedDocument tDoc) {
				Map<Integer, Double> rawVect = vectorize.toVector(tDoc.features);
				return MapToLabeledPoint(vectorize.getDimension(), label, rawVect);
			}
		});
	}
	
	private static JavaPairRDD<String,LabeledPoint> MapTokenizedDocumentToTrackableVectors(double label, TDIDFVectorize vectorize, JavaRDD<TokenizedDocument> docsRDD) {
		return 
		docsRDD
		.mapToPair(new PairFunction<TokenizedDocument, String, LabeledPoint>() {
			public Tuple2<String, LabeledPoint> call(TokenizedDocument tDoc) {
				Map<Integer, Double> rawVect = vectorize.toVector(tDoc.features);
				LabeledPoint lp = MapToLabeledPoint(vectorize.getDimension(), label, rawVect);
				return new Tuple2<String, LabeledPoint>(tDoc.docId, lp);
			}
		});
	}
	
	private static ClassifierVectorModel SparcTrain(CommandHelper cmd, 
			java.util.function.Function<JavaRDD<LabeledPoint>, ClassificationModel> trainer) throws Exception {
		List<WebpageDocument> medDocs = WikiXmlDirectoryToWebpages(cmd.GetMedicalPath());
		List<WebpageDocument> nonMedDocs = WikiXmlDirectoryToWebpages(cmd.GetNonMedicalPath());
		
	    // Build an RDD of all features
	    JavaRDD<TokenizedDocument> medTokenizedDocs = MapWebpagesToTokenizedDocuemnts (medDocs, cmd.ShouldStem());    
	    JavaRDD<TokenizedDocument> nonMedTokenizedDocs = MapWebpagesToTokenizedDocuemnts (nonMedDocs, cmd.ShouldStem());

	    JavaRDD<TokenizedDocument> allDocs = medTokenizedDocs.union(nonMedTokenizedDocs);
	    Double numOfDocs = (double) allDocs.count();
	    System.out.println("got " + numOfDocs + " num of docs");
	    
	    // Transform [doc1, {t1: x1, t2: x2}, doc2, {t1: y1,}...] to [t1: [doc1, doc2]], [t2: [doc1]]....
	    // first: [doc1, (term1, x1)], [doc1, (term2, x1)], ...
	    JavaPairRDD<String, Tuple2<String, Integer>> flatDocsTerms = 
	    	medTokenizedDocs.flatMapToPair(new PairFlatMapFunction<TokenizedDocument, String, Tuple2<String, Integer>>() {
			public Iterable<Tuple2<String, Tuple2<String, Integer>>> call(TokenizedDocument tDoc) throws Exception {
				List<Tuple2<String, Tuple2<String, Integer>>> ret = new ArrayList<Tuple2<String, Tuple2<String, Integer>>>();
				
				for(Map.Entry<String, Integer> t : tDoc.features.entrySet())
					ret.add(new Tuple2<String, Tuple2<String, Integer>>
							(tDoc.docId, new Tuple2<String, Integer>(t.getKey(), t.getValue())));
				
				return ret;
			}
		});

	    // Inverse document term: Transform to [(t1, doc1), (t1, doc2)]
	    JavaPairRDD<String, Double> termsIDFs = 
		flatDocsTerms
		/* <t1, [<doc1, <t1, x1>>, <doc2, <t1, y1>> ..>, ... */ 
		.groupBy(new Function<Tuple2<String,Tuple2<String,Integer>>, String>() {
			public String call(Tuple2<String, Tuple2<String, Integer>> docTerm){
				return docTerm._2._1;
			}})
		/* <t1, idf_t1>, <t2, idf_t2>, ...*/
		.mapToPair(new PairFunction<Tuple2<String,Iterable<Tuple2<String,Tuple2<String,Integer>>>>, String, Double>() {
			public Tuple2<String, Double> call(Tuple2<String, Iterable<Tuple2<String, Tuple2<String, Integer>>>> termEntry) {
				int termOccurences=0;
				for (Tuple2<?,?> e : termEntry._2) termOccurences++;
				
				if (termOccurences < 3) return new Tuple2<String, Double>("", numOfDocs);
				return new Tuple2<String, Double>(termEntry._1, numOfDocs / termOccurences);
			}
		}).distinct();
	    
	    
	    Map<String, Double> localTermsIDFs = termsIDFs.collectAsMap();
	    TDIDFVectorize vectorize = new TDIDFVectorize(localTermsIDFs);
	    
	    System.out.println("Train dimension in R^" + vectorize.getDimension());
	     
		JavaRDD<LabeledPoint> medLabeledSamples = MapTokenizedDocumentToVectors(Medical, vectorize, medTokenizedDocs);
		JavaRDD<LabeledPoint> nonMedLabeledSamples = MapTokenizedDocumentToVectors(Other, vectorize, nonMedTokenizedDocs);
		JavaRDD<LabeledPoint> allSamples = medLabeledSamples.union(nonMedLabeledSamples);
	    
		
		ChiSqSelectorModel chiSqrModel = chiSqrSelector.fit(allSamples.rdd());
		JavaRDD<LabeledPoint> allSamplesFiltered =
		allSamples
		.map(new Function<LabeledPoint, LabeledPoint>() {
			public LabeledPoint call(LabeledPoint lp) {
				return new LabeledPoint(lp.label(), chiSqrModel.transform(lp.features()));
			}
		});
		
		JavaRDD<LabeledPoint>[] splittedSample = allSamplesFiltered.randomSplit(new double[] {1-testSample, testSample});
		JavaRDD<LabeledPoint> trainingSet = splittedSample[0];
		JavaRDD<LabeledPoint> testSet = splittedSample[1];
		
		// train and predict
		ClassificationModel model = trainer.apply(trainingSet);
	    JavaPairRDD<Double, Double> labelPrediction = 
		testSet
		.mapToPair(new PairFunction<LabeledPoint, Double, Double>() {
			public Tuple2<Double, Double> call(LabeledPoint p) {
				return new Tuple2<Double, Double>(p.label(), model.predict(p.features()));
			}
		});
	    
	    Accumulator<Integer> truePositive = sparkConf.accumulator(0, "TruePositive");
	    Accumulator<Integer> totalTrue = sparkConf.accumulator(0, "TotalPositive");
	    Accumulator<Integer> falsePositive = sparkConf.accumulator(0, "FalsePositive");
	    Accumulator<Integer> totalFalse = sparkConf.accumulator(0, "TotalFalse");
	    
		labelPrediction.foreach(new VoidFunction<Tuple2<Double,Double>>() {
			public void call(Tuple2<Double, Double> prediction) {
				if (prediction._1.compareTo(prediction._2) == 0) {
					totalTrue.add(1);
					if (prediction._1 == Medical)
						truePositive.add(1);
				} else {
					totalFalse.add(1);
					if (prediction._1 == Medical)
						falsePositive.add(1);
				}
			}
		});
	    
		PrintSummary(totalTrue.value(), truePositive.value(), totalFalse.value(), falsePositive.value());
	    
	    return new ClassifierVectorModel(model, chiSqrModel, vectorize);
	}
	
	private static LabeledPoint MapToLabeledPoint(int dim, double label, Map<Integer, Double> map) {
		int[] indeces = map.keySet().stream().mapToInt(i->i).toArray();
		double[] values = map.values().stream().mapToDouble(d->d).toArray(); 
		return new LabeledPoint(label,
					Vectors.sparse(dim, indeces, values));
	}
	
	private static List<WebpageDocument> WarcRecordToWebpage(String warcPath) {
		List<WebpageDocument> ret = new ArrayList<WebpageDocument>();
		try {
			DataInputStream stream = new DataInputStream(new FileInputStream(warcPath));
			int nullUris = 0;
			WarcRecord record;
			while ((record = WarcRecord.readNextWarcRecord(stream)) != null) {
				try {
					URI uri = record.getUri();
					if (uri != null) {
						ret.add(new WebpageDocument(uri.getHost(), uri.getPath(), ArticleExtractor.INSTANCE.getText(record.getContentUTF8())));
					} else {
						nullUris++;
					}

				} catch (URISyntaxException e) {
					System.err.println("Warning in parsing uri of warc recods: " + record.getWarcUUIDString());
				} catch (IllegalArgumentException e) {
					System.err.println("Warning IllegalArgumentException: " + record.getWarcUUIDString());
					e.printStackTrace();
				} catch (BoilerpipeProcessingException e) {
					System.err.println("Warning BoilerpipeProcessingException: " + record.getWarcUUIDString());
				} 
			}
			
			System.err.printf("Warining: Total Null URIs %d\n", nullUris);
		} catch (FileNotFoundException e) {
			System.err.println("ERROR: Warc file not found");
		} catch (IOException e) {
				System.err.println("ERROR: IO exception in warc file not found");
		}
		
		return ret;
	}
	
	private static List<WebpageDocument> WikiXmlDirectoryToWebpages(String dir) throws ParserConfigurationException, SAXException, IOException {
		List<WebpageDocument> docs = new ArrayList<WebpageDocument>();
		Iterator<File> it = FileUtils.iterateFiles(new File(dir), null, true);
		while(it.hasNext()) {
			docs.addAll(WikiXmlToWebpages(it.next().getPath()));
		}
		
		return docs;
	}
	
	private static List<WebpageDocument> WikiXmlToWebpages(String xmlPath) throws IOException {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		Document document;
		NodeList nodeList;
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			
			FileInputStream source = new FileInputStream(xmlPath);
			document = db.parse(source);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			throw new Error("error in parsing configuration");
		} catch (SAXException e) {
			e.printStackTrace();
			throw new Error("Error in SAX, while parsing " + xmlPath);
		}

	    nodeList = document.getDocumentElement().getChildNodes();
	    
	    List<WebpageDocument> result = new ArrayList<WebpageDocument>();
	    for (int i = 0; i < nodeList.getLength(); i++) {
	    	Node node = nodeList.item(i);
	    	if (node instanceof Element) {
	    		Element eNode = (Element) node;
	    		result.add(new WebpageDocument(null, eNode.getAttribute("title"), eNode.getTextContent(), xmlPath));
	    	} else {
	    		System.out.println("Warning - element " + i);
	    	}
    	}
		
		return result;
	}
	
	private static void TokenizeIfNeeded(CommandHelper cmd) {
		if (!cmd.ShouldTokenizeXml())
			return;
		
		
		List<WebpageDocument> wikiXmls;
		try {
			wikiXmls = WikiXmlToWebpages(cmd.GetTokenizeXmlPath());
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
		for (WebpageDocument webDoc : wikiXmls) {
			TokenizedDocument tDoc = WebpageFeatureFactory.buildFeaturedDocument(webDoc, cmd.ShouldStem());
			for (Map.Entry<String, Integer> token : tDoc.features.entrySet()) {
				System.out.println(token.getKey() + ": " + token.getValue());
			}
		}
	}
	
	private static void PredictIfNeeded(CommandHelper cmd, ClassifierVectorModel naiveBayes) {
		if (!cmd.ShouldPredictWARC())
			return;
		
		// Load non tagged samples
		JavaRDD<TokenizedDocument> toPredictRecords = MapWebpagesToTokenizedDocuemnts(WarcRecordToWebpage(cmd.GetPredicWarcPath()), cmd.ShouldStem());
		
		// Map them to vectors
		JavaPairRDD<String,LabeledPoint> predicted = 
		MapTokenizedDocumentToTrackableVectors(Unsupervised, naiveBayes.vectorFactory, toPredictRecords)
		.filter(new Function<Tuple2<String,LabeledPoint>, Boolean>() {
			public Boolean call(Tuple2<String, LabeledPoint> p) throws Exception {
				LabeledPoint lp = p._2;
				Vector tv = naiveBayes.chiSqrModel.transform(lp.features());
				return naiveBayes.model.predict(tv) == Medical;
			}
		});
		
		System.out.println("Predicted " + predicted.count() + " as med docs out of " + toPredictRecords.count());
		
		List<String> docs = predicted
		.map(new Function<Tuple2<String,LabeledPoint>, String>() {
			public String call(Tuple2<String, LabeledPoint> p) {
				return p._1;
			}
		}).collect();
		
		for (String s : docs) {
			System.out.println(s);
		}
	}
	
	private static void PrintSummary(long totalTrue, long truePositive, long totalFalse, long falsePositive) {
		long falseNegative = totalFalse - falsePositive;
		double prec = (float)truePositive / (float)(truePositive + falsePositive); 
		double recall = (float)truePositive / (float)(truePositive + falseNegative);
		
	    System.out.println("************************************************\n");
	    System.out.println("**** Was false " + totalFalse + " times out of " + (totalFalse + totalTrue) + " ****");
	    System.out.println("**** FP: " + falsePositive + " FN: " + (totalFalse - falsePositive) + " ****");
	    System.out.println("**** Prec: " + prec + " Recall: " + recall  + " ****");
	    System.out.println("\n************************************************");
	}
	
	/*private static List<WebpageDocument> TalFilesToWebpages(String[] dirs) throws Exception {
		List<WebpageDocument> docs = new ArrayList<WebpageDocument>();
		for (String dir : dirs) {
			docs.addAll(simpleTxtFilesToWebpages(dir));
		}
		
		return docs;
	}
	
	private static List<WebpageDocument> simpleTxtFilesToWebpages(String directory) throws Exception {
		String[] ext = new String[] { "txt" };
		File dirFile = new File(directory);
		Iterator<File> it = FileUtils.iterateFiles(dirFile, ext, true);
		
		List<WebpageDocument> docs = new ArrayList<WebpageDocument>();
		
		while (it.hasNext()) {
			File fPath = it.next();
			String fileContent = FileUtils.readFileToString(fPath);
			docs.add(new WebpageDocument(fPath.getParentFile().getName(), fPath.getName(), fileContent)); 
		}
		
		return docs;
	}*/

}
