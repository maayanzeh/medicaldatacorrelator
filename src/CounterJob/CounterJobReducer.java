package CounterJob;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class CounterJobReducer extends Reducer <Text, IntWritable, Text, LongWritable>{
	@Override
	public void reduce (Text k,  Iterable<IntWritable> values, Context c) throws IOException, InterruptedException {
		long sum = 0;
		for (IntWritable v : values) {
			sum += v.get();
		}
		
		c.write(k, new LongWritable(sum));
	}
}
