package CounterJob;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.commons.lang3.StringUtils;

import edu.cmu.lemurproject.WritableWarcRecord;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.ArticleExtractor;


public class CounterJobMapper extends Mapper<LongWritable, WritableWarcRecord, Text, IntWritable>{
	
	private static double PmiThreshold = 0.7;
	private List<String> UMLS;
	
	
	@Override
	public void setup(Context c) throws IOException {
		UMLS = new ArrayList<String>();		
		/* Read UMLS file */
		InputStreamReader vocabStream = new InputStreamReader(
				CounterJobMapper.class.getResourceAsStream("/resources/med_ukwac.vocab"));
		
		BufferedReader vocabReader = new BufferedReader (vocabStream);
		String l;
		while ( (l = vocabReader.readLine()) != null) {
			String[] vocabEntry = l.split("\t");
			if (vocabEntry.length == 2) {
				Double pmi = Double.parseDouble(vocabEntry[1]);
				if (pmi > PmiThreshold) {
					UMLS.add(vocabEntry[0].toLowerCase());
				}
			}
		}
	}
	
	@Override
    public void map(LongWritable k, WritableWarcRecord v, Context c)
    		throws IOException, InterruptedException {
		
		try {
			String decodedContent = v.getRecord().getContentUTF8();
			if (null == decodedContent) {
				// log
				return;
			}
			
			String contentText = ArticleExtractor.INSTANCE.getText(decodedContent);
			if (null == contentText) {
				// log
				return;
			}
			c.getCounter("General", "Docs").increment(1);
			// normalize search
			contentText = contentText.toLowerCase();
			
			int occurancesCount;
			for (String t : UMLS) {
				occurancesCount = StringUtils.countMatches(contentText, t);
				if (0 < occurancesCount){
					c.write(new Text(t), new IntWritable(occurancesCount));
				}
			}
		} catch (BoilerpipeProcessingException e) {
			// TODO: increase failure counter
			// TODO: log
			e.printStackTrace();
			return;
		} catch (Exception e) {
			// TODO: increase failure counter
			// TODO: log
			e.printStackTrace();
			
			return;
		}
	}
}
